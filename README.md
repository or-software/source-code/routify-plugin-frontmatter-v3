### Usage

In your config insert:

```js
routify({
  plugins: [
    'routify-plugin-frontmatter-v3'
  ]
})
```

Frontmatter metadata can be found in ``meta.frontmatter`` of the respective file.

To change default extensions (md and svx)
``"routify-plugin-frontmatter-v3": { "extensions": ["md"] }``


#### Note
This plugin does not strip frontmatter from your file. It only reads it and adds it to your metadata.
