console.log('routify-plugin-frontmatter-v3 loaded...');
const YAML = require('yaml')
const defaults = { extensions: ['md', 'svx'] }

 
​export​ ​default​ ​(​)​ ​=>​ ​(​{ 
​    ​beforeUrlChange​: ​(​{​ route ​}​)​ ​=>​ ​{ 
​        ​const​ ​fragments​ ​=​ ​route​.​allFragments;
​        ​fragments​.​forEach​(​fragment​ ​=>​ ​{ 
​            ​​fragment​.​node​.​meta;
​        ​}​);
​        ​return​ ​true;
​    ​}​,
​}​)

module.exports = (middlewares, pl, options) => {
    options = { ...defaults, ...options }
    return {
        name: 'routify-plugin-frontmatter-v3',
        build: ({ instance }) => {
            instance.middlewares
        }
    };
};
    // insert frontmatter metadata after meta is created
    const index = middlewares.findIndex(mw => mw.name === 'applyMetaToFiles')
    middlewares.splice(index + 1, 0, {
        name: "routify-plugin-frontmatter",
        middleware
    })

    return middlewares

    async function middleware(payload) { await walkFiles(payload.tree) }

    async function walkFiles(file) {
        if (file.children && file.children.length) {
            await Promise.all(file.children.map(walkFiles))
        }
        if (options.extensions.includes(file.ext)) {
            const body = require('fs').readFileSync(file.absolutePath, 'utf8')
            const match = body.match(/^---(((.)|[\s\S])+?)^---/m)            
            if (match) file.meta.frontmatter = YAML.parse(match[1])
        }
    }
